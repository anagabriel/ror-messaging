# AQUA - Messaging
## 1. Purpose
This is the second project within AQUA’s onboarding process for software engineers. It will be a simple messaging web application written in Ruby on Rails that is to be deployed on Heroku. The web application will include user authentication. All of the source code will be stored on a repo through GitHub.

### 1.1 Requirements
* Build a simple messaging system with user authentication
  * PostgreSQL
  * Brakeman (gem for security)
  * Rubycritic (gem for cleaner code)
* Commit your code to github
* Push your code to Heroku.com
* Ruby: 2.4.0
* Rails: 5.1.4

## 2. Design Outline
Since this is a web application, it will use a client-server model as its architecture and Ruby on Rails as its framework. I will, also, include service worker (for Chrome, Firefox, and Opera) that will sit between the cache and the client. The database will use PosgreSQL.

### 2.1 Components:
* Web Client - UI that displays a calculator that takes user input to produce an output; evaluation of the expression will happen on the server and respond with the answer
  * Service Worker - sits between the UI and the client’s cache to manage offline requests
  * HTTP Cache - filters and stores information retrieved from the server
* Server - receives requests from client to display the calculator, and sends the proper HTTP response containing the calculator
* Database - receives query from server, and sends results to server

### 2.2 Diagram:
![diagram](images/diagram.png)

## 3. Design Issues

### 3.1 Functional Issues

#### Issue: What will be displayed on the homepage?
* __Option 1__: User login/creation widget
* Option 2: User inbox, if cookies are present, otherwise user login/creation widget

I decided to just force the user to login every time the window refreshes, or the homepage is accessed.

#### Issue: How will users create a new message?
* __Option 1__: User will press a button to open a popup window with input fields
* Option 2: User will press a button a new view is loaded onto the page with input fields

I decided to go with this option, because canceling a popup will be easier to handle than trying to reload the previous view.

#### Issue: How will users read a message?
* __Option 1__: User click will popup message info
* Option 2: User click will load a view containing message info

I decided to go with this option, because canceling a popup will be easier to handle than trying to reload the previous view.  

#### Issue: How will users reply to a message?
* __Option 1__: An input field will be at the bottom of message info with a send button
* Option 2: User clicks reply to popup a reply input box

I decided to go with this option, because it works with any option for reading a message.

### 3.2 Non-Functional Issues

#### Issue: What web application architecture will be used?
* __Option 1__: MVC
* Option 2: Flux

Since I am using Ruby on Rails, I’m going to use an MVC architecture when implementing this web application.

#### Issue: What web application framework will be used?
* __Option 1__: Ruby on Rails (Ruby 2.4.0 & Rails 5.1.4)
* Option 2: Node.js

Since this is supposed to written in Ruby on Rails, I am using Ruby on Rails as my web application framework. The Ruby version will be 2.4.0, and, for Rails, 5.1.4.

#### Issue: Where will the web application be deployed?
* __Option__ 1: Heroku
* Option 2: AWS
* Option 3: GCP

Since one of the requirement for the web application is to use Heroku, I’ve decided to go with Heroku as the location for deployment.

## 4. Design Details

### 4.1 Database Diagram
![database](images/database.png)

### 4.2 User Interaction Sequence Diagram
![sequence](images/sequence.png)

### 4.3 User Interface Mockups

Sign up:
![creation](images/creation.png)

Sign in:
![login](images/login.png)

Inbox:
![inbox](images/inbox.png)

Send Message:
![send](images/send.png)

Read Message:
![read](images/read.png)
