Rails.application.routes.draw do
  root 'messaging#index'
  get 'messaging/index'

  resources :users
  resources :messages

  match '/signin' => 'messaging#signin', via: :post
  match '/signup' => 'messaging#signup', via: :post
  match '/message' => 'messaging#message', via: :post
  match '/reply' => 'messaging#reply', via: :post
  match '/chain' => 'messaging#chain', via: :post
end
