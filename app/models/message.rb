class Message < ApplicationRecord
  validates :body, presence: true
  validates :subject, presence: true
  validates :to, presence: true
end
