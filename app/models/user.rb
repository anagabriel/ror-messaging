class User < ApplicationRecord
  attr_accessor :password_confirmation
  validates_uniqueness_of :username
  validates :password, presence: true,
            length: { minimum: 8, maximum: 20 }
  validates :username, presence: true
  validates_confirmation_of :password
end
