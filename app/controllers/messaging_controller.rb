class MessagingController < ApplicationController
  def index
    @user = User.new
  end

  def signin
    @user = User.where('username = ? AND password = ?',
                        user_params[:username], user_params[:password])

    if @user.length == 1
      @user = @user[0]
      new_chain
      inbox
    else
      @user = User.new
      @user.errors.add(:username, :invalid, message: "& Password combo doesn't exist!")
      render 'messaging/index.html.erb'
    end
  end

  def signup
    @user = User.new(user_params)
    @user.save
    render 'messaging/index.html.erb'
  end

  def message
    @message = Message.new(message_params)
    @user = User.new
    @user.username = @message.from

    if @message.save
      new_chain
      inbox
    else
      error
    end
  end

  def reply
    @chain = Chain.new(chain_params)
    @user = User.new
    @user.username = @chain.from
    @root = @chain.root

    if @chain.save
      fill_chain
      inbox
    else
      @message = Message.new
      error
    end
  end

  def chain
    @user = User.new
    @user.username = params[:username]
    @root = params[:root]
    fill_messages
    fill_chain
    inbox
  end

private
  def user_params
    params.require(:user).permit(:username, :password, :password_confirmation)
  end

  def message_params
    params.require(:message).permit(:to, :from, :subject, :body, :next)
  end

  def chain_params
    params.require(:chain).permit(:from, :body, :root)
  end

  def fill_messages
    @messages = Message.where('"to" = ? OR "from" = ?',
                                  @user.username, @user.username)
    @message = Message.new
    @message.from = @user.username
  end

  def inbox
    fill_messages
    render 'messaging/inbox.html.erb'
  end

  def error
    @messages = Message.where('"to" = ? OR "from" = ?',
                              @user.username, @user.username)
    render 'messaging/inbox.html.erb'
  end

  def new_chain
    @chain = Chain.new
    @chain.from = @user.username
    @chains = nil
    @subject = nil
    @first = Message.new
  end

  def fill_chain
    new_chain
    @chain.root = @root
    @chains = Chain.where('root = ?', @root)
    @subject = Message.where('id = ?', @root)[0].subject
    @first.body = Message.where('id = ?', @root)[0].body
    @first.from = Message.where('id = ?', @root)[0].from
  end
end
