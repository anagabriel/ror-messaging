class CreateChains < ActiveRecord::Migration[5.1]
  def change
    create_table :chains do |t|
      t.integer :root
      t.integer :next
      t.text :body
      t.string :from

      t.timestamps
    end
  end
end
